package com.citi.training.trades.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class tests the {@link com.citi.training.trades.rest.TradeController} class.
 * @author Administrator
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TradeControllerTests {

    private static final Logger LOG = 
    		LoggerFactory.getLogger(TradeControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService mockTradeService;

    /**
     * This method tests the getAllTrades() method.
     * @throws Exception Thrown if an issue occurs.
     */
    @Test
    public void getAllTrades_returnsList() throws Exception {
        when(mockTradeService.getAllTrades()).thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc.perform(get("/trades")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        LOG.info("Result from TradeService.getAllTrades: " +
                    result.getResponse().getContentAsString());
    }

    /**
     * This method tests the createTrade() method.
     * @throws Exception Thrown if an issue occurs.
     */
    @Test
    public void createTrade_returnsCreated() throws Exception {
        Trade testTrade = new Trade(5, "GOOG", 99999.99, 200);

        this.mockMvc
                .perform(post("/trades").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated()).andReturn();
        LOG.info("Result from TradeService.create");
    }

    /**
     * This method tests the deleteTrade(int id) method.
     * @throws Exception Thrown if an issue occurs.
     */
    @Test
    public void deleteTrade_returnsOK() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/trades/5"))
                                       .andExpect(status().isNoContent())
                                       .andReturn();

        LOG.info("Result from TradeService.deleteTrade: " +
                    result.getResponse().getContentAsString());
    }

    /**
     * This method tests the getTrade(int id) method.
     * @throws Exception Thrown if an issue occurs.
     */
    @Test
    public void getTrade_returnsOK() throws Exception {
    	Trade testTrade = new Trade(10, "APPL", 59999.99, 100);

        when(mockTradeService.getTrade(testTrade.getId())).thenReturn(testTrade);

        MvcResult result = this.mockMvc.perform(get("/trads/10"))
                                       .andExpect(status().isOk()).andReturn();

        LOG.info("Result from TradeService.getTrade: " +
                    result.getResponse().getContentAsString());
    }

}
