package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * This tester class tests the {@link com.citi.training.trades.model.Trade} object
 * @author Administrator
 *
 */
public class TradeTests {

	private int id = 20;
	private String stock = "APPL";
	private double price = 50;
	private int volume = 100;
	private Trade testTrade;
	private String testString;
	
	/**
	 * This method initializes the {@link com.citi.training.trades.model.Trade} object 
	 * and String value needed to test the methods.
	 */
	@Before
	public void setUpObject() {
		testTrade = new Trade(id, stock, price, volume);
		testString = testTrade.toString();
	}
	
	/**
	 * This method tests the constructor for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeConstructor() {
		assertEquals(id, testTrade.getId());
		assert(stock.equals(testTrade.getStock()));
		assert(price == testTrade.getPrice());
		assertEquals(volume, testTrade.getVolume());
	}
	
	/**
	 * This method tests the getId() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeGetId() {
		assertEquals(id, testTrade.getId());
	}
	
	/**
	 * This method tests the setId() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeSetId() {
		int newId = 30;
		testTrade.setId(newId);
		assertEquals(newId, testTrade.getId());
	}
	
	/**
	 * This method tests the getStock() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeGetStock() {
		assert(stock.equals(testTrade.getStock()));
	}
	
	/**
	 * This method tests the setStock() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeSetStock() {
		String newStock = "GOOG";
		testTrade.setStock(newStock);
		assertEquals(newStock, testTrade.getStock());
	}
	
	/**
	 * This method tests the getPrice() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeGetPrice() {
		assert(price == testTrade.getPrice());
	}
	
	/**
	 * This method tests the setPrice() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeSetPrice() {
		int newPrice = 75;
		testTrade.setPrice(newPrice);
		assert(newPrice == testTrade.getPrice());
	}
	
	/**
	 * This method tests the getVolume() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeGetVolume() {
		assertEquals(volume, testTrade.getVolume());
	}
	
	/**
	 * This method tests the setVolume() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeSetVolume() {
		int newVolume = 200;
		testTrade.setVolume(newVolume);
		assertEquals(newVolume, testTrade.getVolume());
	}
	
	/**
	 * This method tests the overridden toString() method for the 
	 * {@link com.citi.training.trades.model.Trade} class.
	 */
	@Test
	public void test_TradeToString() {
		assert(testString.contains(new Integer(id).toString()));
		assert(testString.contains(new String(stock)));
		assert(testString.contains(new Double(price).toString()));
		assert(testString.contains(new Integer(volume).toString()));
	}
}
