package com.citi.training.trades.dao;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

/**
 * This class tests the {@link com.citi.training.trades.dao.MysqlTradeDao} class
 * @author Administrator
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

	private Trade testTrade;
	
	@Autowired
	MysqlTradeDao mysqlTradeDao;
	
	/**
	 * This method initializes the {@link com.citi.training.trades.model.Trade} 
	 * object needed to test the methods required.
	 */
	@Before
	public void test_setUpTradeObjects() {
		testTrade = new Trade(-1, "GOOG", 10.0, 20);
	}
	
	/**
	 * This method tests the createTrade() method and the 
	 * getAllTrades() method.
	 */
	@Test
	@Transactional
	public void test_createAndGetAll() {
		mysqlTradeDao.createTrade(testTrade);
		assertTrue(mysqlTradeDao.getAllTrades().size() >= 1);
	}
	
	/**
	 * This method tests the getTrade() method where it should return the 
	 * correct values.
	 */
	@Test
	@Transactional
	public void test_getTradeSuccess() {
		Trade createdTrade = mysqlTradeDao.createTrade(testTrade);
		Trade trade = new Trade();
		trade = mysqlTradeDao.getTrade(createdTrade.getId());
		assert(trade.getStock().equals("GOOG"));
		assert(trade.getPrice() == 10.0);
		assert(trade.getVolume() == 20);
	}
	
	/**
	 * This method tests the getTrade() method where the 
	 * TradeNotFoundException should be thrown.
	 */
	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void test_getTradeNotSuccessful() {
		mysqlTradeDao.getTrade(-100);
	}
	
	/**
	 * This method tests the deleteTrade() method where the 
	 * TradeNotFoundException should be thrown as the trade 
	 * has been successfully removed.
	 */
	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void test_deleteTradeSuccessful() {
		Trade trade = mysqlTradeDao.createTrade(testTrade);
		mysqlTradeDao.deleteTrade(trade.getId());
		mysqlTradeDao.getTrade(trade.getId());
	}
	
	/**
	 * This method tests the deleteTrade() method where the 
	 * TradeNotFoundException should be thrown as the trade 
	 * does not exist.
	 */
	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void test_deleteTradeNotSuccessful() {
		mysqlTradeDao.deleteTrade(-1);
	}
}
