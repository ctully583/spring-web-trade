package com.citi.training.trades.model;

/**
 * This class sets up the {@link com.citi.training.trades.model.Trade} object.
 * @author Administrator
 *
 */
public class Trade {

	private int id;
	private String stock;
	private double price;
	private int volume;
	
	/**
	 * This is an empty constructor for initializing a 
	 * {@link com.citi.training.trades.model.Trade} object.
	 */
	public Trade() {}
	
	/**
	 * This constructor initializes the stock, price and volume 
	 * for a {@link com.citi.training.trades.model.Trade} object. 
	 * The ID is defaulted to -1.
	 * @param stock The Stock Symbol.
	 * @param price The Dollar Amount at which the trade will be made.
	 * @param volume The Number of stocks to be traded.
	 */
	public Trade(String stock, double price, int volume) {
		this(-1, stock, price, volume);
	}
	
	/**
	 * This constructor initializes the id, stock, price and volume 
	 * for a {@link com.citi.training.trades.model.Trade} object.
	 * @param id The ID value of the Trade.
	 * @param stock The Stock Symbol.
	 * @param price The Dollar Amount at which the Trade will be made.
	 * @param volume The Number of Stocks to be traded.
	 */
	public Trade(int id, String stock, double price, int volume) {
		this.id = id;
		this.stock = stock;
		this.price = price;
		this.volume = volume;
	}

	/**
	 * This getter retrieves the ID value for the Trade.
	 * @return The ID value.
	 */
	public int getId() {
		return id;
	}

	/**
	 * This setter changes the ID value for the Trade.
	 * @param id The new ID value.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * This getter retrieves the Stock value for the Trade.
	 * @return The Stock Symbol.
	 */
	public String getStock() {
		return stock;
	}

	/**
	 * This setter changes the Stock value for the Trade.
	 * @param stock The Stock Symbol.
	 */
	public void setStock(String stock) {
		this.stock = stock;
	}

	/**
	 * This getter retrieves the Price value for the Trade.
	 * @return The Dollar Amount at which the Trade will be made.
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * This setter changes the Price value for the Trade.
	 * @param price The Dollar Amount at which the Trade will be made.
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * This getter retrieves the Volume value for the Trade.
	 * @return The Number of Stocks to be traded.
	 */
	public int getVolume() {
		return volume;
	}

	/**
	 * This setter changes the Volume value for the Trade.
	 * @param volume The Number of Stocks to be traded.
	 */
	public void setVolume(int volume) {
		this.volume = volume;
	}

	/**
	 * This overridden method converts the {@link com.citi.training.trades.model.Trade} 
	 * object to a String representation.
	 * @return
	 */
	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", price=" + price + ", volume=" + volume + "]";
	}
}
