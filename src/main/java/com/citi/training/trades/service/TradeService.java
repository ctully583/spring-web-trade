package com.citi.training.trades.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.model.Trade;

/**
 * This class provides the service required to run the 
 * {@link com.citi.training.trades.dao.MysqlTradeDao} methods.
 * @author Administrator
 */
@Component
public class TradeService {

	@Autowired
	private TradeDao tradeDao;
	
	/**
	 * This method retrieves all Trades.
	 * @return The List of {@link com.citi.training.trades.model.Trade} objects.
	 */
	public List<Trade> getAllTrades() {
		return tradeDao.getAllTrades();
	}
	
	/**
	 * This method retrieves a specified Trade.
	 * @param id The ID value of the Trade.
	 * @return The {@link com.citi.training.trades.model.Trade} object.
	 */
	public Trade getTrade(int id) {
		return tradeDao.getTrade(id);
	}
	
	/**
	 * This method inserts a new {@link com.citi.training.trades.model.Trade} object 
	 * into the database.
	 * @param trade The {@link com.citi.training.trades.model.Trade} object details.
	 * @return The newly created {@link com.citi.training.trades.model.Trade} object.
	 */
	public Trade createTrade(Trade trade) {
		if (trade.getStock().length() > 0) {
			return tradeDao.createTrade(trade);
		}
		throw new RuntimeException("Invalid Parameter: trade stock: " + 
		trade.getStock());
	}
	
	/**
	 * This method removes a {@link com.citi.training.trades.model.Trade} object 
	 * from the database.
	 * @param id The ID value of the Trade.
	 */
	public void deleteTrade(int id) {
		tradeDao.deleteTrade(id);
	}
}
