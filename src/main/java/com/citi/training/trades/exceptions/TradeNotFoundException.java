package com.citi.training.trades.exceptions;

/**
 * This class defines the Exception thrown when a 
 * {@link com.citi.training.trades.model.Trade} has not been found.
 * @author Administrator
 */
@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {

	/**
	 * This constructor initializes the TradeNotFoundException.
	 * @param message The error message displayed to the user.
	 */
	public TradeNotFoundException(String message) {
		super(message);
	}
}