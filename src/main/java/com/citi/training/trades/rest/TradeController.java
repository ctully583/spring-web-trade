package com.citi.training.trades.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;

/**
 * REST interface class for {@link com.citi.training.trades.model.Trade} domain object.
 * 
 * @author Administrator
 * @see Trade
 */
@RestController
@RequestMapping("/trades")
public class TradeController {

	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
	
    @Autowired
    TradeService tradeService;

    /**
     * Find all {@link com.citi.training.trades.model.Trade} objects.
     * @return List of {@link com.citi.training.trades.model.Trade} objects.
     */
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> getAllTrades(){
    	LOG.debug("getAllTrades() method is being called");
        return tradeService.getAllTrades();
    }

    /**
     * Find an {@link com.citi.training.trades.model.Trade} by it's integer id.
     * @param id The id of the trade to find
     * @return {@link com.citi.training.trades.model.Trade} that was found or HTTP 404.
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Trade getTrade(@PathVariable int id) {
    	LOG.debug("getTrade(int id) method is being called");
        return tradeService.getTrade(id);
    }

    /**
     * Create an {@link com.citi.training.trades.model.Trade} object.
     * @param employee The {@link com.citi.training.trades.model.Trade} object details
     * @return The creation response
     */
    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trade> createTrade(@RequestBody Trade trade) {
    	LOG.debug("createTrade(Trade trade) method is being called");
        return new ResponseEntity<Trade>(tradeService.createTrade(trade),
                                            HttpStatus.CREATED);
    }

    /**
     * Remove an {@link com.citi.training.trades.model.Trade} by it's integer id.
     * @param id The id of the trade to find
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteTrade(@PathVariable int id) {
    	LOG.debug("deleteTrade(int id) method is being called");
        tradeService.deleteTrade(id);
    }
}
