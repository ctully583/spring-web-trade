package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

/**
 * This class implements the methods declared in the 
 * {@link com.citi.training.trades.dao.TradeDao} class.
 * @author Administrator
 *
 */
@Component
public class MysqlTradeDao implements TradeDao {

	@Autowired
	JdbcTemplate tpl;
	
	/**
	 * This method returns all of the {@link com.citi.training.trades.model.Trade} 
	 * objects in the database.
	 * @return The List of {@link com.citi.training.trades.model.Trade} objects.
	 */
	@Override
	public List<Trade> getAllTrades() {
		return tpl.query("SELECT id, stock, price, volume FROM trade;", 
				new TradeMapper());
	}

	/**
	 * This method retrieves a single {@link com.citi.training.trades.model.Trade} object 
	 * from the database.
	 * @param id The ID value of the Trade.
	 * @return The {@link com.citi.training.trades.model.Trade} object.
	 */
	@Override
	public Trade getTrade(int id) {
		List<Trade> trades = 
				tpl.query("SELECT id, stock, price, volume FROM trade WHERE id=?;", 
				new Object[] {id}, new TradeMapper());
		if (trades.size() == 0) {
			throw new TradeNotFoundException("Trade with id " + id + " has not been found");
		}
		return trades.get(0);
	}

	/**
	 * This method creates a new {@link com.citi.training.trades.model.Trade} object and 
	 * adds it to the database.
	 * @param trade The {@link com.citi.training.trades.model.Trade} object details.
	 * @return The newly created {@link com.citi.training.trades.model.Trade} object.
	 */
	@Override
	public Trade createTrade(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		tpl.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) 
					throws SQLException {
				PreparedStatement preparedStatement = 
						connection.prepareStatement(
								"INSERT INTO trade (stock, price, volume) VALUES (?, ?, ?)", 
								Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, trade.getStock());
				preparedStatement.setDouble(2, trade.getPrice());
				preparedStatement.setInt(3, trade.getVolume());
				return preparedStatement;
			}
		}, keyHolder);
		trade.setId(keyHolder.getKey().intValue());
		return trade;
	}

	/**
	 * This method removes a {@link com.citi.training.trades.model.Trade} object 
	 * from the database.
	 * @param id The ID value of the Trade.
	 */
	@Override
	public void deleteTrade(int id) {
		getTrade(id);
		tpl.update("DELETE FROM trade WHERE id = ?", id);		
	}

	/**
	 * This class maps the Trade Results retrieved from the database to 
	 * a new {@link com.citi.training.trades.model.Trade} object.
	 * @author Administrator
	 * @see {@link com.citi.training.trades.model.Trade}
	 */
	private static final class TradeMapper implements RowMapper<Trade> {
		
		/**
		 * This method maps the SQL query results to a new 
		 * {@link com.citi.training.trades.model.Trade} object
		 * @return The new {@link com.citi.training.trades.model.Trade} object
		 */
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"), rs.getString("stock"), 
					rs.getDouble("price"), rs.getInt("volume"));
		}
	}
}
