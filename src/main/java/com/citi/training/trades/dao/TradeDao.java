package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trade;

/**
 * This interface will declare the methods for 
 * the {@link com.citi.training.trades.dao.MysqlTradeDao} class
 * @author Administrator
 *
 */
public interface TradeDao {

	/**
	 * This method will retrieve all 
	 * {@link com.citi.training.trades.model.Trade} objects 
	 * stored in the database.
	 * @return The list of {@link com.citi.training.trades.model.Trade} objects.
	 */
	List<Trade> getAllTrades();
	
	/**
	 * This method will retrieve a {@link com.citi.training.trades.model.Trade} object 
	 * stored in the database.
	 * @param id The ID value of the Trade.
	 * @return The {@link com.citi.training.trades.model.Trade} object
	 */
	Trade getTrade(int id);
	
	/**
	 * This method will add a new {@link com.citi.training.trades.model.Trade} object 
	 * in the database.
	 * @param trade The details of the new Trade.
	 * @return The newly created {@link com.citi.training.trades.model.Trade} object.
	 */
	Trade createTrade(Trade trade);
	
	/**
	 * This method will remove a {@link com.citi.training.trades.model.Trade} object 
	 * from the database.
	 * @param id The ID value of the Trade.
	 */
	void deleteTrade(int id);
}